<?php
	header("HTTP/1.1 404 Not Found");
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.loading{
	position: fixed; 
	width: 100%;
	left: 0; 
	height: 100%;
	top: 0; 
	background: #ddd; 
	z-index: 2001; 
	font-family: monospace;
	font-weight: bolder;
} 
.muter{ 
	position: absolute; 
	width: 80px; 
	height: 80px; 
	margin-left: -40px; 
	top: 50px; 
	left: 50%; 
	border: ridge 14px;
	box-sizing: border-box; 
	border-radius: 50%; 
	border-color: #123 #fff; 
	/*animation: rotate 2s infinite linear; */

	text-align: center;
	font-size: 100px;
	overflow: visible;
	padding: auto;
}

@keyframes rotate{ 
	from{ transform: rotate(0deg); } 
	50%{ transform: rotate(180deg); } 
	to{ transform: rotate(360deg); } 
}
</style>

<div class="loading">
	<div class="muter" style="border-color: transparent;width: 400px;margin-left: -200px;top: 20px;text-decoration: underline;">4&nbsp;&nbsp;4</div>
	<div class="muter" style="transform: rotate(45deg);"></div>
	<div style="text-align: center;margin-top: 150px;font-size: 20px;">Halaman tidak ditemukan</div>
	<div  style="text-align: center;margin: 10px;font-size: 30px;"><a href="/">Kembali ke Home</a></div>
</div>
