<?php
session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");

error_reporting(0);

require 'db.php';

$rute = substr($_SERVER["REQUEST_URI"], 1);
$req = [];
foreach (array('?', '#') as $v) {
	$r = explode( $v , $rute);
	$rute = $r[0];
	$req[$v] = null;
	if(isset($r[1])){
		$req[$v] = $r[1];
	}
}

$req['?'] = explode('&', $req['?']);
foreach ($req['?'] as $k => $v) {
	unset($req['?'][$k]);
	$v = explode('=', $v);
	if($v[0]=='')continue;
	if(!isset($v[1])){
		$v[1]=null;
	}
	$v[1] = urldecode($v[1]);
	$req['?'][$v[0]] = $v[1];
}
$_GET=$req['?'];
$myurl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$login=null;

$canonical = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

// JSON REQUEST
// var_dump($_SERVER['CONTENT_TYPE']);
if($_SERVER['CONTENT_TYPE']=='application/json'){
	$_POST = json_decode(file_get_contents('php://input'), true);
}

if(0){}


else if(prefix('login')){
	include 'page/login.php';
}
else if(prefix('register')){
	include 'page/register.php';
}
else if(prefix('test')){
	include 'page/usercheck.php';
}
else if(prefix('feed')){
	include 'page/feed.php';
}
else if(prefix('novel')){
	include 'page/noveldetail.php';
}
else if(prefix('chapter')){
	include 'page/novelchapter.php';
}
else if(prefix('library')){
	include 'page/userlibrary.php';
}

else if(prefix('search')){
	include 'page/novelsearch.php';
}

else if(prefix('')){
	header("Content-Type: text/plain; charset=utf-8");
	echo 'End point API Final Project React Native Candradimuka Jabar Coding Camp 2021';
}

/*ERROR N FRIENDS*/
else if(prefix('404') ) {
	header("Content-Type: text/html; charset=utf-8");
	include 'error/404.php';
}

else {
	header("Content-Type: text/html; charset=utf-8");
	include 'error/404.php';
}


///////////////////////////////

function prefix($x,$p=0){
	global $rute, $canonical;
	$loc1 = explode('/', strtolower($rute.'/'));
	if(isset($loc1[$p]) && strtolower($x) == $loc1[$p] ){
		$canonical .="/".$loc1[$p];
		return true;
	}
	return false;
}

function get_rute($p=0){
	global $rute;
	$loc1 = explode('/', strtolower($rute));
	return isset($loc1[$p]) ? $loc1[$p] : false;
}


function get_ruteP($p=0){
	global $rute;
	$loc1 = explode('/', ($rute));
	return isset($loc1[$p]) ? $loc1[$p] : false;
}

function masuk($x){
	global $rute;
	return strpos($rute, $x) !== false;
}

function build_param($arr){
	foreach ($arr as $k => $v) {
		$loc1[] = $k.'='.urlencode($v);
	}
	return implode('&', $loc1);
}

function get_user($u, $p){
	global $_db;
	$u = strtolower($u);
	$salt = "__RakauPlain__";
	$hasil = $_db -> query("SELECT * FROM User WHERE lower(email)='"._norm($u)."' AND password='".hash("sha256", $p.$salt.$u)."' ");
	return mysqli_fetch_array($hasil,1);
}

function get_user_by_email($u){
	global $_db;
	$u = strtolower($u);
	$salt = "__RakauPlain__";
	$hasil = $_db -> query("SELECT * FROM User WHERE lower(email)='"._norm($u)."'  ");
	return mysqli_fetch_array($hasil,1);
}

function get_user_by_token($token){
	global $_db;
	$hasil = $_db -> query("SELECT * FROM User WHERE token='"._norm($token)."'  ");
	return mysqli_fetch_array($hasil,1);
}
function get_user_reset($u, $t){
	global $_db;
	$u = strtolower($u);
	$salt = "__RakauPlain__";
	$hasil = $_db -> query("SELECT * FROM User WHERE lower(email)='"._norm($u)."' AND reset_token='"._norm($t)."' AND reset_timeout IS NOT NULL AND reset_timeout>=CURRENT_TIMESTAMP ");
	return mysqli_fetch_array($hasil,1);
}

function new_user($u,$n, $p){
	global $_db;
	$u = strtolower($u);
	$salt = "__RakauPlain__";
	$insert = $_db -> query("INSERT INTO User (`email`, `name`,`password`, `is_verified`) VALUES ('"._norm($u)."','"._norm($n)."','".hash("sha256", $p.$salt.$u)."',0) ");

	return $insert;
}

function update_user($u, $p, $t){
	global $_db;
	$u = strtolower($u);
	$salt = "__RakauPlain__";
	$hasil = $_db -> query("UPDATE User SET reset_timeout=NULL, password='".hash("sha256", $p.$salt.$u)."' WHERE lower(email)='"._norm($u)."' AND reset_token='"._norm($t)."' ");
	return mysqli_fetch_array($hasil,1);
}

function get_login($id){
	global $_db;
	$hasil = $_db -> query("SELECT * FROM User WHERE user_id='"._norm($id)."' ");
	return mysqli_fetch_array($hasil,1);
}

function set_token($id, $token){
	global $_db;
	$hasil = $_db -> query("UPDATE User SET token='"._norm($token)."' WHERE user_id='"._norm($id)."' ");
	return 0;
	return mysqli_fetch_array($hasil,1);
}

function is_post_valid($a){
	foreach ($a as $v) {
		if(!isset($_POST[$v])){
			return false;
		}
	}
	return true;
}

function is_get_valid($a){
	foreach ($a as $v) {
		if(!isset($_GET[$v])){
			return false;
		}
	}
	return true;
}


function uploadImage($file){
	$file_max_weight = 5000000; //limit the maximum size of file allowed (2000Mb)
	$ok_ext = ['jpg','png','jpeg']; // allow only these types of files
	$destination = './uploads/'; // where our files will be stored

	$filename = explode(".", $file["name"]); 
	$file_name = $file['name']; // file original name
	$file_name_no_ext = isset($filename[0]) ? $filename[0] : null; // File name without the extension
	$file_extension =strtolower($filename[count($filename)-1]);
	$file_weight = $file['size'];
	$file_type = $file['type'];

	$b64_type = pathinfo($file['tmp_name'], PATHINFO_EXTENSION);
	$b64_data = file_get_contents($file['tmp_name']);
	// $base64 = 'data:image/' . $b64_type . ';base64,' . base64_encode($b64_data);
	$base64 = 'data:' . $file_type . ';base64,' . base64_encode($b64_data);
	// $w = getimagesize($file['tmp_name'])[0];
	// $h = getimagesize($file['tmp_name'])[1];

	$return='OK';

	do{
		if( $file['error'] != 0 ){
			$return = "Terjadi kesalahan";
			break;
		}
		if(!in_array($file_extension, $ok_ext)){
			$return = "Format file tidak didukung";
			break;
		}
		if( $file_weight > $file_max_weight ){
			$return = "Ukuran file teralu besar";
			break;
		}
		// $fileNewName = $new_name.".".$file_extension;
		// if(move_uploaded_file($file['tmp_name'], $destination.$fileNewName)){
		// 	$return = "OK";
		// }else{
		// 	$return = "Gagal mengunggah";
		// }

	}while(0);

	return [$return, $base64];

}


?>