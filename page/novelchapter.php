<?php

$return = [
	'success' => 'false',
	'error' => 'terjadi kesalahan',
];
$error = '';

$user_id = 0;
if(isset($_POST['token'])){
	$login = get_user_by_token($_POST['token']);
	if($login!=null){
		$user_id = $login['user_id'];
	}
}
/*

next chapter
SELECT * FROM `Chapter` WHERE chapter_id<>1 AND number>=1.1 AND book_id=1 ORDER BY number ASC LIMIT 1 

prev chapter
SELECT * FROM `Chapter` WHERE chapter_id<>4 AND number<='1.2' AND book_id=1 ORDER BY number DESC LIMIT 1 
*/

function get_chapter_content($book_id, $chapter_id){
	global $_db;
	$book_id = _norm($book_id);
	$chapter_id = _norm($chapter_id);

	$hasil = $_db -> query("SELECT chapter_id, book_id, number, title, content, created_at FROM `Chapter` WHERE book_id={$book_id} AND chapter_id={$chapter_id} ORDER BY number DESC, chapter_id DESC ");
	return mysqli_fetch_array($hasil,1);
}

function get_next_chapter($book_id, $chapter_id, $number){
	global $_db;
	$book_id = _norm($book_id);
	$chapter_id = _norm($chapter_id);
	$number = _norm($number);

	$hasil = $_db -> query("SELECT chapter_id, number, title FROM `Chapter` WHERE chapter_id<>{$chapter_id} AND number>='{$number}' AND book_id={$book_id} ORDER BY number ASC LIMIT 1");
	return mysqli_fetch_array($hasil,1);
}

function get_prev_chapter($book_id, $chapter_id, $number){
	global $_db;
	$book_id = _norm($book_id);
	$chapter_id = _norm($chapter_id);
	$number = _norm($number);

	$hasil = $_db -> query("SELECT chapter_id, number, title FROM `Chapter` WHERE chapter_id<>{$chapter_id} AND number<='{$number}' AND book_id={$book_id} ORDER BY number DESC LIMIT 1");
	return mysqli_fetch_array($hasil,1);
}

function update_read_history($user_id, $chapter_id){
	global $_db;
	$user_id = _norm($user_id);
	$chapter_id = _norm($chapter_id);
	$hasil = $_db -> query("INSERT INTO `Read_History` (`user_id`, `chapter_id`, `last_read`) VALUES ('{$user_id}', '{$chapter_id}', CURRENT_TIMESTAMP )  ON DUPLICATE KEY UPDATE `last_read` = CURRENT_TIMESTAMP");
}

function content_format($el){
	// $el = htmlspecialchars($el, ENT_QUOTES);
	$el = utf8_encode($el);
	$el = trim($el);
	return $el;
}

function content_filter($el){
	return strlen($el)>2;
}

if(get_rute(1)!==false && get_rute(2)!==false){
	$book_id = intval(get_rute(1));
	$chapter_id = intval(get_rute(2));
	$chapter = get_chapter_content($book_id,$chapter_id);


	if($chapter){
		unset($return['error']);
		$return['success'] = 'true';
		$content = explode("\n", $chapter['content']);
		$content = array_map('content_format', $content);
		$content = array_filter($content, 'content_filter');

		$chapter['content'] = $content;
		$return['data'] = $chapter;

		$return['data']['next_chapter'] = get_next_chapter($book_id, $chapter_id, $chapter['number']);
		$return['data']['prev_chapter'] = get_prev_chapter($book_id, $chapter_id, $chapter['number']);

		if($login!=null){
			update_read_history($user_id, $chapter_id);
		}

	}else{
		$return['error'] = 'Chapter tidak ditemukan';
	}

}else{
	$return['error'] = 'Request salah';
}

echo json_encode($return);

?>