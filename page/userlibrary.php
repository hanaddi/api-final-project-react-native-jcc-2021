<?php

$return = [
	'success' => 'false',
	'error' => 'terjadi kesalahan',
];
$error = '';

$user_id = 0;
if(isset($_POST['token'])){
	$login = get_user_by_token($_POST['token']);
	if($login!=null){
		// login success
		$return['success'] = 'true';
		unset($return['error']);
		$user_id = $login['user_id'];

		$return['data']=[
			'liked' => get_liked($user_id),
			'history' => get_last_read($user_id),
		];
	}
	else{
		$return['error'] = "User tidak ditemukan";
	}
}
else{
	$return['error'] = "Method salah";
}

echo json_encode($return);

function get_liked($user_id){
	global $_db;
	$user_id = _norm($user_id);
	$hasil = $_db -> query("SELECT Book.book_id, Book.title, cover, writer FROM `Favorite`, `Book` WHERE Favorite.book_id = Book.book_id AND user_id={$user_id} ");
	$result = [];
	while($row = mysqli_fetch_array($hasil,1)){
		$result[] = $row;
	}
	return $result;
}
function get_last_read($user_id){
	global $_db;
	$user_id = _norm($user_id);
	$hasil = $_db -> query("SELECT MAX(Read_History.last_read) as read_time, Book.book_id, Book.title, cover, writer FROM `Read_History`, `Chapter`, `Book` WHERE Read_History.chapter_id=Chapter.chapter_id AND Chapter.book_id = Book.book_id AND Read_History.user_id={$user_id} GROUP BY Book.book_id ORDER BY read_time DESC LIMIT 30");
	$result = [];
	while($row = mysqli_fetch_array($hasil,1)){
		$result[] = $row;
	}
	return $result;
}

?>