<?php

$return = [
	'success' => 'false',
	'error' => 'terjadi kesalahan',
];
$error = '';

// $user_id = 0;
// if(isset($_POST['token'])){
// 	$login = get_user_by_token($_POST['token']);
// 	if($login!=null){
// 		// login success
// 		$return['success'] = 'true';
// 		unset($return['error']);
// 		$user_id = $login['user_id'];

// 		$return['data']=[
// 			'liked' => get_liked($user_id),
// 			'history' => get_last_read($user_id),
// 		];
// 	}
// 	else{
// 		$return['error'] = "User tidak ditemukan";
// 	}
// }
// else{
// 	$return['error'] = "Method salah";
// }


if(isset($_GET['q'])){
	$return['success'] = 'true';
	unset($return['error']);
	$return['data']=get_search($_GET['q']);
}
echo json_encode($return);

function get_search($search){
	global $_db;
	$search = _norm(strtolower($search));
	$hasil = $_db -> query("SELECT book_id, title, cover, writer FROM `Book` WHERE LOWER(title) like '%{$search}%' ");
	$result = [];
	while($row = mysqli_fetch_array($hasil,1)){
		$result[] = $row;
	}
	return $result;
}

?>