<?php

$return = [
	'success' => 'false',
	'error' => 'terjadi kesalahan',
];
$error = '';

$user_id = 0;
if(isset($_POST['token'])){
	$login = get_user_by_token($_POST['token']);
	if($login!=null){
		$user_id = $login['user_id'];
	}
}


function get_book($id){
	$id = _norm($id);
	global $_db;
	$hasil = $_db -> query("SELECT book_id, title, cover, writer, summary, created_at FROM `Book` WHERE book_id='{$id}' ");
	return mysqli_fetch_array($hasil,1);
}

function get_loves($user_id, $book_id){
	$user_id = _norm($user_id);
	$book_id = _norm($book_id);
	global $_db;
	$hasil = $_db -> query("SELECT IFNULL(SUM(user_id={$user_id}), 0) as loved, COUNT(book_id) as loves FROM `Favorite` where book_id={$book_id} ");
	return mysqli_fetch_array($hasil,1);
}

function set_love($user_id, $book_id){
	$user_id = _norm($user_id);
	$book_id = _norm($book_id);
	global $_db;
	$hasil = $_db -> query("INSERT INTO `Favorite` (`book_id`, `user_id`) VALUES ('{$book_id}', '{$user_id}')");
	// return mysqli_fetch_array($hasil,1);
}

function set_unlove($user_id, $book_id){
	$user_id = _norm($user_id);
	$book_id = _norm($book_id);
	global $_db;
	$hasil = $_db -> query("DELETE FROM `Favorite` WHERE `Favorite`.`book_id` = {$book_id} AND `Favorite`.`user_id` = {$user_id}");
	// return mysqli_fetch_array($hasil,1);
}

function get_chapter($book_id){
	global $_db;
	$book_id = _norm($book_id);
	// $hasil = $_db -> query("SELECT chapter_id, book_id, number, title, content, created_at FROM `Chapter` WHERE book_id={$book_id} ORDER BY number DESC, chapter_id DESC ");
	$hasil = $_db -> query("SELECT chapter_id, number, title FROM `Chapter` WHERE book_id={$book_id} ORDER BY number DESC, chapter_id DESC ");
	$result = [];
	while($row = mysqli_fetch_array($hasil,1)){
		$result[] = $row;
	}
	return $result;
}


if(get_rute(1)!==false){
	$book = get_book(get_rute(1));
	if($book){
		unset($return['error']);
		$return['success'] = 'true';
		$return['data'] = $book;
		$return['data']['logged_in'] = $login!=null?'1':'0';
		if($login!=null){
			if(isset($_POST['like'])){
				$like = intval($_POST['like']);
				if($like>0){
					// like
					set_love($user_id, get_rute(1));
				}else{
					// unlike
					set_unlove($user_id, get_rute(1));
				}
			}
		}

		$loves = get_loves($user_id, get_rute(1));
		$return['data']['loved'] = $loves['loved'];
		$return['data']['loves'] = $loves['loves'];
		$return['data']['chapters'] = get_chapter($book['book_id']);

	}else{
		$return['error'] = 'Novel tidak ditemukan';
	}
}else{
	$return['error'] = 'Request salah';
}
echo json_encode($return);


?>