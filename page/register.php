<?php

$token = "";
$return = [
	'success' => 'false',
	'error' => 'method salah',
];
$error = '';

if(isset($_SESSION['login'])){
	echo '<meta http-equiv="refresh" content="0; URL=/" />';
}else
if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

	$d_name = isset($_POST['name'])?implode('\"', explode('"', $_POST['name'])):'';
	$d_user = isset($_POST['email'])?implode('\"', explode('"', $_POST['email'])):'';
	$d_pass = isset($_POST['pass'])?implode('\"', explode('"', $_POST['pass'])):'';
	$d_repass = isset($_POST['repass'])?implode('\"', explode('"', $_POST['repass'])):'';

	do {
		if(!is_post_valid(['email','pass','repass','name'])){
			// $return['data'] = $_POST;
			$error = "Form tidak lengkap";
			break;
		}
		if(strlen($_POST['name'])>100){
			$error = "Nama terlalu panjang";
			break;
		}
		$is_name_valid = preg_match('/^[A-Za-z ]{3,}$/', $_POST['name'], $matches, PREG_OFFSET_CAPTURE);
		if(!($is_name_valid && $is_name_valid>0)){
			$error = "Nama tidak valid";
			break;
		}
		if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$error = "Alamat email tidak valid";
			break;
		}
		if(strlen($_POST['email'])>100){
			$error = "Alamat email terlalu panjang";
			break;
		}
		if(strlen($_POST['pass'])>100){
			$error = "Password terlalu panjang";
			break;
		}
		if(strlen($_POST['pass'])<6){
			$error = "Password terlalu pendek";
			break;
		}
		if($_POST['pass']!=$_POST['repass']){
			$error = "Password tidak sesuai";
			break;
		}
		$user = get_user_by_email($_POST['email']);

		if(isset($user)){
			$error = "Alamat email sudah terdaftar";
			break;
		}
		$insert = new_user($_POST['email'],$_POST['name'],$_POST['pass']);

		if(!$insert){
			$error = "Terjadi kesalahan";
			break;
		}

		$hasil = $_db -> query("SELECT * FROM User WHERE email='"._norm($_POST['email'])."'");
		if($hasil && $hasil->num_rows>0){
			$data = mysqli_fetch_array($hasil,1);
			unset($data['password']);
			unset($data['type']);
			unset($data['token']);
			unset($return['error']);
			$return['success'] = 'true';
			$return['data'] = $data;
			echo json_encode($return);
			return;
		}else{
			$error = "Terjadi kesalahan";
			break;
		}




	} while (0);
	if($error != ''){
		$return['error'] = $error;
		echo json_encode($return);
	}

}else{

	// $char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_1234567890";
	// $len = 20;
	// while (strlen($token)<$len) {
	// 	$idx = rand(0,strlen($char)-1);
	// 	$token.=$char[$idx];
	// }
	// setcookie("token", $token);
	echo json_encode($return);
}





?>